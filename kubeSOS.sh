#!/bin/bash

while getopts 'n:r:h' flag; do
  case "${flag}" in
    n) namespace="-n ${OPTARG}" ;;
    r) release="${OPTARG}" ;;
    h) echo "Usage: $0 [-n namespace ] [ -r release]" >&2 ; exit 0 ;;
  esac
done

if [ -z "$release" ] ; then
  release="gitlab"
fi

timestamp="$(date +%d%m%Y%H%M%S)"
archive_dir="./kubesos-$timestamp"
mkdir -p $archive_dir


# Verify requirements and write versions to file
echo "Getting kubectl version..."
kubectl_check="$(kubectl version 2> /dev/null)"

if [ -n "$kubectl_check" ]; then
    echo "$kubectl_check" > $archive_dir/kubectl-check
  else
    echo -e "ERROR: Could not load kubectl version, do you have kubectl installed?\n"
    exit 1
fi

# Only populate variable if version is between 1.14 and 1.19.
kubectl_version="$(kubectl version --client --short | grep -oh 'v.\..[4-9]\..')"
if [ -z "$kubectl_version" ]; then
  echo "ERROR: This script requires kubectl v1.14+"
  exit 1
fi

echo "Getting helm version..."
helm_version="$(helm version 2> /dev/null)"

if [ -n "$helm_version" ]; then
    echo "$helm_version" > $archive_dir/helm-version 
  else
    echo -e "WARN: Could not load helm version, do you have helm installed?\n"
fi

echo "Getting GitLab chart version..."
chart_version="$(helm ls $release 2> /dev/null)"

if [ -n "$chart_version" ]; then
    echo "$chart_version" > $archive_dir/chart-version 
  else
    echo -e "WARN: Unable to retrieve chart version\n"
fi


echo "Get pods..."
kubectl get pods $namespace > $archive_dir/pods
echo "Describe pods..."
kubectl describe pods $namespace > $archive_dir/describe
echo "Describe nodes..."
kubectl describe nodes $namespace > $archive_dir/nodes
echo "Describe configmaps..."
kubectl describe configmaps $namespace > $archive_dir/configmaps
echo "Retrieve list secrets (Content not included)..."
kubectl get secrets $namespace > $archive_dir/secrets
echo "Retrieve chart values..."
helm get values $release > $archive_dir/values.yaml

# Get all app names from the app label
app_names=($(kubectl get pods $namespace --output=jsonpath={.items..metadata.labels.app}))
# Retrieve distinct app names from the app_names array above for situations where there are multiple pods for an app
distinct_app_names=($(printf "%s\n" "${app_names[@]}" | sort -u | tr '\n' ' '))

# Retrieve logs for each component in array
for app in "${distinct_app_names[@]}"
do
  echo "Retrieve logs for $app..."
  kubectl logs $namespace -l app=$app --all-containers=true --max-log-requests=50 --tail=10000 > $archive_dir/$app.log
done

archive_file="$archive_dir.tar.gz"
tar -czf $archive_file $archive_dir
echo "Archive file $archive_file created."
