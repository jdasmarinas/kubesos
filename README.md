# KubeSOS

`kubectl` and `helm` wrapper to retrieve GitLab cluster configuration and logs from GitLab Cloud Native chart deployments

#### Requirements
[kubectl client v1.14+](https://kubernetes.io/docs/tasks/tools/install-kubectl/)

[helm 2.12+](https://helm.sh/docs/intro/quickstart/) (helm v3 is not currently supported)

#### Usage

| Flags | Description | Required   | Default
| :---- | :---------- | :--------- | :------
| `-n`  | namespace   | No | "default"
| `-r`  | helm chart release | No | "gitlab"

Download and execute:

```
chmod +x kubeSOS.sh
./kubeSOS.sh [flags]
```

Or use `curl`:

```
curl https://gitlab.com/gitlab-com/support/toolbox/kubesos/raw/master/kubeSOS.sh [flags] | bash
``` 

Data will be archived to `kubesos-<timestamp>.tar.gz`
